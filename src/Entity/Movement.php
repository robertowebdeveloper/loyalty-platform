<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovementRepository")
 * @ORM\Table(indexes={@Index(columns={"deleted_at"}),@Index(columns={"point"}),@Index(columns={"deleted_at","type"})})
 */
class Movement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Card", inversedBy="movements")
     */
    private $Card;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"comment":"10 => Movimento di carico, 20 => movimento di scarico , 30 => ricarica punti negozio"})
     * @Assert\NotBlank(message="Type is requested")
     * @Assert\Choice({10,20,30} , message="Type is not valid")
     */
    private $type;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"comment":"10.10 => Movimento di carico per acquisto, 10.20 => movimento di carico per regalo da negozio , 20.10 => movimento di scarico per rimborso buono , 20.20 => movimento di scarico per errore , 30.10 => ricarica punti negozio per acquisto , 30.20 => ricarica negozio per regalo punti"})
     * @Assert\Positive(message="subType is not valid")
     */
    private $subType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="movements")
     */
    private $Shop;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="Point is request")
     * @Assert\PositiveOrZero(message="Point must be equal to or greater than 0")
     */
    private $point;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, columnDefinition="DATETIME on update CURRENT_TIMESTAMP")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $deletedReason;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCard(): ?Card
    {
        return $this->Card;
    }

    public function setCard(?Card $Card): self
    {
        $this->Card = $Card;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSubType(): ?int
    {
        return $this->subType;
    }

    public function setSubType(?int $subType): self
    {
        $this->subType = $subType;
        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->Shop;
    }

    public function setShop(?Shop $Shop): self
    {
        $this->Shop = $Shop;

        return $this;
    }

    public function getPoint(): ?float
    {
        return $this->point;
    }

    public function setPoint(float $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedReason(): ?string
    {
        return $this->deletedReason;
    }

    public function setDeletedReason(?string $deletedReason): self
    {
        $this->deletedReason = $deletedReason;

        return $this;
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Movement;
use App\Entity\Shop;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $Company = new Company();
        $Company->setName('Azienda di prova srl');
        $Company->setVat('01234567891');
        $manager->persist($Company);

        for($i=1;$i<10;$i++){
            $Shop = new Shop();
            $Shop->setCompany($Company);
            $Shop->setName('Negozio di prova ' . $i);
            $Shop->setAddress('Via Dei Calzaiuoli, 1');
            $Shop->setZip('50100');
            $Shop->setCity('Firenze');
            $Shop->setConversion( rand(1,5) );
            $manager->persist($Shop);

            $Movement = new Movement();
            $Movement->setShop($Shop);
            $Movement->setPoint(1000000);
            $Movement->setType(30);
            $Movement->setSubType(10);
            $manager->persist($Movement);
        }

        $manager->flush();
    }
}

<?php

namespace App\Services;

use App\Entity\Card;
use App\Entity\Customer;
use App\Entity\Movement;
use App\Entity\Shop;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MovementService
{
    const
        movTypeLoad = 10,
        movTypeUnload = 20,
        movTypeShopLoad = 30
    ;

    private $container;
    private $session;

	public function __construct(ContainerInterface $container , SessionInterface $session)
	{
		$this->container = $container;
		$this->em = $container->get('doctrine')->getManager();
		$this->session = $session;
	}

    /**
     * Ritorna il saldo di una card dato il suo codice
     */
	public function getBalanceFromCardCode( string $code ): ?int
    {
        $cardRepo = $this->em->getRepository(Card::class);
        $Card = $cardRepo->findOneBy([
            'code'  => $code
        ]);
        if( $Card ) {
            return $this->getBalanceFromCard( $Card );
        }
        return null;
    }

    /**
     * Ritorna il saldo di una card dato il suo utente
     */
    public function getBalanceFromCustomer( Customer $Customer ): ?int
    {
        foreach( $Customer->getCards() as $Card ){
            if( is_null( $Card->getDeletedAt() ) ){
                return $this->getBalanceFromCard( $Card );
            }
        }
        return null;
    }

    /**
     * Ritorna il saldo di una card
     */
    public function getBalanceFromCard( Card $Card ): ?int
    {
        $movementRepo = $this->em->getRepository(Movement::class);

        $balance = 0;
        foreach([ self::movTypeLoad , self::movTypeUnload ] as $movType){
            $res = $movementRepo->createQueryBuilder('m')
                ->select('SUM(m.point) AS tot')
                ->where('m.type = :type')
                ->andWhere('m.deletedAt IS NULL')
                ->andWhere('m.Card = :card')
                ->setParameters([
                    'card'      => $Card,
                    'type'      => $movType,
                ])
                ->getQuery()
                ->getOneOrNullResult()
            ;
            $tmpBalance = $res['tot'] ?? 0;

            $factor = $movType==self::movTypeLoad ? 1 : -1;//fattore che indica se si deve aggiungere o sottrarre
            $balance += $tmpBalance * $factor;
        }
        return $balance;
    }

    public function getShopBalance( Shop $Shop ): ?int
    {
        $movementRepo = $this->em->getRepository(Movement::class);

        $balance = 0;
        foreach([ self::movTypeLoad , self::movTypeShopLoad ] as $movType){
            $res = $movementRepo->createQueryBuilder('m')
                ->select('SUM(m.point) AS tot')
                ->where('m.type = :type')
                ->andWhere('m.deletedAt IS NULL')
                ->andWhere('m.Shop = :shop')
                ->setParameters([
                    'shop'      => $Shop,
                    'type'      => $movType,
                ])
                ->getQuery()
                ->getOneOrNullResult()
            ;
            $tmpBalance = $res['tot'] ?? 0;

            $factor = $movType==self::movTypeShopLoad ? 1 : -1;//fattore che indica se si deve aggiungere o sottrarre
            $balance += $tmpBalance * $factor;
        }
        return $balance;
    }

    /**
     * Valido i tipi di movimento di spesa / ricarico
     */
    public function isMovTypeValidForSpent( int $type )
    {
        return ( $type == self::movTypeLoad || $type == self::movTypeUnload );
    }
}
<?php

namespace App\Services;

class UtilityService
{
    /*
    private $container;
    private $session;

	public function __construct()
	{
		$this->container = $container;
		$this->em = $container->get('doctrine')->getManager();
		$this->session = $session;
	}
    */

	public function getRandomCode( int $length=5 , bool $onlyNumber = false )
    {
        for($code='',$i=0;$i<$length;$i++) {
            $rand = (string) rand(0, 10000) * time();
            $str = $onlyNumber ? $rand : md5($rand);
            $code .= substr($str, rand(0 , strlen($str)-1 ),1);
        }
        return strtoupper( $code );
    }
}
<?php

namespace App\Repository;

use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Shop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shop[]    findAll()
 * @method Shop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    public function findByArray( ?array $criteria , ?array $orderBy ): ?array
    {
        $list = $this->findBy( $criteria , $orderBy );
        if(! is_null($list) ) {
            $arrayReturn = [];
            foreach ($list as $k => $v) {
                $arrayReturn[] = [
                    'id' => $v->getId(),
                    'company_id' => $v->getCompany()->getId(),
                    'name' => $v->getName(),
                    'lat' => $v->getLat(),
                    'lng' => $v->getLng(),
                    'address' => $v->getAddress(),
                    'zip' => $v->getZip(),
                    'city' => $v->getCity(),
                    'tel' => $v->getTel(),
                    'fax' => $v->getFax(),
                    'mobile' => $v->getMobile(),
                    'email' => $v->getEmail(),
                    'toShow' => $v->getToShow(),
                    'conversion' => $v->getConversion(),
                    'created_at' => $v->getCreatedAt(),
                    'updated_at' => $v->getUpdatedAt(),
                    'deleted_at' => $v->getDeletedAt(),
                ];
            }

            return $arrayReturn;
        }
        return null;
    }

    // /**
    //  * @return Shop[] Returns an array of Shop objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Shop
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

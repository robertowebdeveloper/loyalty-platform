<?php

namespace App\Controller;

use App\Entity\Card;
use App\Entity\Customer;
use App\Entity\Movement;
use App\Entity\Shop;
use App\Services\MovementService;
use App\Services\UtilityService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    public function __construct(UtilityService $utilityService)
    {
        $this->utilityService = $utilityService;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return new Response("API Loyalty Platform v{$_ENV['APP_VERSION']}");
    }

    /**
     * @Route("/signup", name="signup", methods={"POST"})
     * Registrazione utente
     */
    public function signup(Request $request , ValidatorInterface $validator , UserPasswordEncoderInterface $passwordEncoder ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_FORBIDDEN;

        $Customer = new Customer();
        $Customer->setFirstname( $request->request->get('firstname', null) );
        $Customer->setLastname( $request->request->get('lastname', null) );
        $Customer->setEmail( $request->request->get('email', null) );
        $Customer->setMobile( $request->request->get('mobile', null) );
        if( $pw = $request->request->get('password', null) )
            $Customer->setPassword( $passwordEncoder->encodePassword( $Customer , $request->request->get('password') ) );

        $birthday = $request->request->get('birthday' , null);
        $Customer->setBirthday( ( $birthday ? new \DateTime($birthday) : null) );

        $Customer->setGender( $request->request->get('gender', null) );
        $Customer->setAddress( $request->request->get('address' , null) );
        $Customer->setCity( $request->request->get('city' , null) );
        $Customer->setZip( $request->request->get('zip', null) );
        $Customer->setApiToken( sha1( $request->request->get('email') . rand(1,10000) ) );
        $Customer->setRoles( ['ROLE_USER'] );
        //Nella demo l'attivazione è immediata, senza invio della e-mail di conferma
        #$Customer->setActivationCode( $this->utilityService->getRandomCode() );
        $Customer->setActivatedAt( new \DateTime() );

        $errors = $validator->validate($Customer);

        if( count($errors) == 0 ){
            $em = $this->getDoctrine()->getManager();
            $em->persist($Customer);

            /*
             * Creo la Card collegata all'utente
             */
            $Card = new Card();
            $Card->setCustomer( $Customer );
            $Card->setActivatedAt( new \DateTime() );
            //Genero codice della card e verifico che non sia già stato utilizzato
            $cardRepo = $em->getRepository( Card::class );
            do{
                $codeCard = $this->utilityService->getRandomCode( $_ENV['CARD_CODE_LENGTH'] , true);
                $flag = $cardRepo->findOneBy([
                    'code'  => $codeCard
                ]);
            }while( $flag );
            $Card->setCode( $codeCard );
            $em->persist($Card);

            $em->flush();
            $_status = JsonResponse::HTTP_OK;
        }else{
            $res = $this->setErrors($res , $errors );
            $_status= JsonResponse::HTTP_NOT_ACCEPTABLE;
        }

        return new JsonResponse( $res , $_status );
    }

    /**
     * @Route("/update_customer", name="updateCustomer", methods={"PATCH"})
     * @IsGranted("ROLE_USER")
     * Modifica utente
     */
    public function updateCustomer(Request $request , ValidatorInterface $validator , UserPasswordEncoderInterface $passwordEncoder ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $em = $this->getDoctrine()->getManager();

        $Customer = $em->getRepository(Customer::class)->findOneBy([
            'email'     => $request->request->get('email')
        ]);
        if( $Customer ) {
            $Customer->setFirstname($request->request->get('firstname'));
            $Customer->setLastname($request->request->get('lastname'));
            $Customer->setMobile($request->request->get('mobile'));
            if( $pw = $request->request->get('password' , null) )
                $Customer->setPassword( $passwordEncoder->encodePassword($Customer, $pw) );

            $birthday = $request->request->get('birthday', null);
            $Customer->setBirthday(($birthday ? new \DateTime($birthday) : null));

            $Customer->setGender($request->request->get('gender', null));
            $Customer->setAddress($request->request->get('address', null));
            $Customer->setCity($request->request->get('city', null));
            $Customer->setZip($request->request->get('zip', null));

            $errors = $validator->validate($Customer);

            if (count($errors) == 0) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($Customer);
                $em->flush();
                $_status = JsonResponse::HTTP_OK;
            } else {
                $res = $this->setErrors($res, $errors);
            }
        }else{
            $res['message'] = 'Customer doesn\'t found';
        }

        return new JsonResponse( $res, $_status );
    }

    /**
     * @Route("/signin", name="signin", methods={"POST"})
     * Login utente
     */
    public function signin(Request $request , UserPasswordEncoderInterface $passwordEncoder ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $Customer = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
            'email'             => $request->request->get('email'),
            'deletedAt'         => null,
            'activationCode'    => null,
        ]);

        if(
            $Customer
            &&
            $passwordEncoder->isPasswordValid($Customer, $request->request->get('password') )
        ){
            $_status = JsonResponse::HTTP_OK;
            $birthday = $Customer->getBirthday();
            $res['data'] = [
                'id'            => $Customer->getId(),
                'firstname'     => $Customer->getFirstname(),
                'lastname'      => $Customer->getLastname(),
                'mobile'        => $Customer->getMobile(),
                'email'         => $Customer->getEmail(),
                'gender'        => $Customer->getGender(),
                'birthday'      => ( $birthday ? $birthday->format('Y-m-d') : null ),
                'address'       => $Customer->getAddress(),
                'zip'           => $Customer->getZip(),
                'city'          => $Customer->getCity(),
                'apitoken'      => $Customer->getApiToken(),
                'card'          => '',
            ];

            //Trovo la card attiva collegata all'utente
            foreach ($Customer->getCards() as $card){
                if(is_null($card->getDeletedAt() ) && empty($res['data']['card']) ){
                    $res['data']['card'] = $card->getCode();
                }
            }

        }else{
            $res['message'] = 'E-mail or password incorrect';
        }

        return new JsonResponse($res, $_status);
    }

    /**
     * @Route("/confirm_customer", name="confirmCustomer", methods={"POST"})
     * Conferma utente da codice di attivazione e ritorna il codice della loyalty card creata per lui
     */
    public function confirmCustomer( Request $request ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $em = $this->getDoctrine()->getManager();
        $customerRepo = $em->getRepository(Customer::class);
        $Customer = $customerRepo->findOneBy([
            'activationCode'    => $request->request->get('code'),
        ]);

        if( $Customer ){
            /*
             * Il Customer è stato trovato dal codice di attivazione per tanto imposto a null l'activationCode e la data/ora di attivazione
             */
            $Customer->setActivationCode();
            $Customer->setActivatedAt( new \DateTime() );
            $em->persist($Customer);

            $em->flush();
            $_status = JsonResponse::HTTP_OK;
        }else{
            $res['message'] = 'Activation code doesn\'t found.';
        }

        return new JsonResponse($res , $_status);
    }

    /**
     * @Route("/get_card_balance/{cardCode}", name="getCardBalance", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * Richiesta saldo punti di una card
     */
    public function getBalance( ?string $cardCode = null , MovementService $movementService ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $cardCode = $cardCode ?? '';
        $balance = $movementService->getBalanceFromCardCode( $cardCode );

        if( is_null($balance) ){
            $res['message'] = 'Card not found';
        }else {
            $_status = JsonResponse::HTTP_OK;
            $res['data']['balance'] = $balance;
        }

        return new JsonResponse($res , $_status);
    }

    /**
     * @Route("/get_shop_balance/{shopId}", name="getShopBalance", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * Richiesta saldo punti di un negozio
     */
    public function getShop( ?int $shopId = null , MovementService $movementService ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $shopId = $shopId ?? 0;
        $Shop = $this->getDoctrine()->getRepository(Shop::class)->find( $shopId );

        if( is_null( $Shop ) ){
            $res['message'] = 'Shop not found';
        }else {
            $balance = $movementService->getShopBalance( $Shop );
            $_status = JsonResponse::HTTP_OK;
            $res['data']['balance'] = $balance;
        }

        return new JsonResponse($res , $_status);
    }

    /**
     * @Route("/add_movement", name="addMovement", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * Aggiunge un movimento e ritorna il saldo della card collegata
     */
    public function addMovement( Request $request , MovementService $movementService , ValidatorInterface $validator ): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $em = $this->getDoctrine()->getManager();

        //Acquisisco Card
        $cardCode = $request->request->get('cardCode' , null);
        $repoCard = $em->getRepository(Card::class);
        $Card = $repoCard->findOneBy([
            'code'  => $cardCode
        ]);
        if( is_null($Card) ){
            $res['message'] = 'Card doesn\'t found';
            return new JsonResponse( $res, $_status );
        }

        //Acquisisco Shop
        $Shop = $em->getRepository(Shop::class)->find( $request->request->get('shopId') );
        if( is_null($Card) ){
            $res['message'] = 'Shop doesn\'t found';
            return new JsonResponse( $res , $_status);
        }

        //Acquisisco spesa effettuata
        $spent = $request->request->get('spent', null);
        if(! $spent>0 ){
            $res['message'] = 'Spent is required';
            return new JsonResponse( $res , $_status);
        }

        //Acquisisco il tipo di movimento
        $movType = $request->request->get('type');
        if(! $movementService->isMovTypeValidForSpent($movType) ){
            $res['message'] = 'Movement type is not valid';
            return new JsonResponse( $res , $_status);
        }

        if( $movType == 10 ){//Operazione di carico punti, verifico i requisiti per la ricarica: il negozio deve avere saldo sufficiente
            $pointToLoad = $spent * $Shop->getConversion();
            $shopBalance = $movementService->getShopBalance( $Shop );
            if( $pointToLoad > $shopBalance ){
                $res['message'] = 'The store points balance is not enough';
                return new JsonResponse( $res , $_status);
            }
        }else if( $movType == 20 ){ //Operazione di scarico punti, verifico i requisiti per il buono sconto: il cliente deve avere saldo sufficiente
            $pointToLoad = $spent;
            $cardBalance = $movementService->getBalanceFromCard( $Card );
            if( $pointToLoad > $cardBalance ){
                $res['message'] = 'Card points balance is not enough';
                return new JsonResponse( $res , $_status);
            }
        }

        $Movement = new Movement();
        $Movement->setShop( $Shop );
        $Movement->setCard( $Card );
        $Movement->setType( $request->request->get('type') );
        $Movement->setSubType( $request->request->get('subtype') );
        $Movement->setPoint( $pointToLoad );

        $errors = $validator->validate($Movement);
        if( count($errors)==0 ){
            $em->persist($Movement);
            $em->flush();
            $_status = JsonResponse::HTTP_OK;
            $res['data']['movementId'] = $Movement->getId();
            $res['data']['cardCode'] = $cardCode;
            $res['data']['cardBalance'] = $movementService->getBalanceFromCard( $Card );
        }else{
            $res['errors'] = $this->setErrors( $res , $errors );
            $res['message'] = 'Verification data';
        }

        return new JsonResponse( $res , $_status);
    }

    /**
     * @Route("/shop_list", name="shopList", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function shopList(): JsonResponse
    {
        $res = $this->getResponse();
        $_status = JsonResponse::HTTP_NOT_ACCEPTABLE;

        $list = $this->getDoctrine()->getRepository(Shop::class)->findByArray([
            'deletedAt'     => null,
        ],[
            'name'          => 'asc'
        ]);

        if( $list ){
            $_status = JsonResponse::HTTP_OK;
            foreach($list as $k => $v){
                $res['data'][] = (array) $v;
            }
            unset($list);
        }

        return new JsonResponse( $res  , $_status);
    }

    /**
     * Imposta la struttura di risposta comune a tutti gli endpoint
     */
    private function getResponse(): array
    {
        return [
            'data'      => [],
            'errors'    => [],
            'message'   => '',
            'debug'     => '',
        ];
    }

    /**
     * Struttura i messaggi di errore
     */
    private function setErrors( array $res , ConstraintViolationList $errors ): array
    {
        foreach ($errors as $k => $error){
            $res['errors'][ $error->getPropertyPath() ] = $error->getMessage();
        }
        return $res;
    }
}

<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return new Response("Loyalty Platform v{$_ENV['APP_VERSION']}");
    }

    public function error()
    {
        return $this->RedirectToRoute('home');
    }
}

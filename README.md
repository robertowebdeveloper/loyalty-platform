**Piattaforma Loyalty**

La piattaforma è rilasciata come open source, basata su Framework Symfony v5.0.

*In questo documento è indicata la procedura di installazione.*

---

**Requisiti**
PHP v7.3+
MySQL v5.7+
Apache o NGinx
Composer installato

1. Clonare il repository
2. Lanciare il comando "composer install"
3. E' necessario impostare alcuni parametri locali della macchina dove risiede l'applicazione.
   Questi parametri sono memorizzati nel file .env.local che è ignorato dal repository per preservare la riservatezza.
   Procedere con i seguenti passaggi:
   1. Creare un file di testo chiamato .env.local
   2. Copiare il seguente contenuto:
       DATABASE_URL=mysql://user:password@host:port/db_name?serverVersion=5.7
   3. Modificare la stringa a secondo della necessità:
       3. "mysql" può essere variato a seconda del motore RDBMS che si vuole utilizzare
       3. "user" indica il nome utente per accedere al database
       3. "password" indica la password dell'utente
       3. "host" indica l'IP o il dominio dove viene eseguito lo RDBMS (es. 127.0.0.1)
       3. "port" indica la porta del database
       3. "db_name" indica il nome del database
       3. "?server_version" indica la versione specifica dello RDBMS
   4. Salvare il file
4. Creare il database lanciando il comando:
    "php bin/console doctrine:database:create"
5. Creare le entità lanciando il comando: "php bin/console doctrine:schema:update --force"
6. Popolare il database con i dati demo con il comando: "php bin/console doctrine:fixtures:load" (confermare il purge)

**Il codice sorgente dell'app mobile per il test di frontend è all'interno della directory "_mobile_app/src" mentre l'APK già pronto da installare su dispositivo Android è "http://loyalty.jmcsoft.it/LoyaltyPlatform.apk"
    
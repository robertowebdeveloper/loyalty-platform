Alloy.m = {
    mainDbName: 'loyaltyplatform',
    //per sviluppo locale: apiUrl: 'http://loyalty.local:8888/api/',
    apiUrl: 'http://loyalty.jmcsoft.it/api/',

    demoShopId: 1,//ID del negozio usato nella demo,

    apiSend: function( action , params , method , callback, failure_callback)
    {
        var _ = this;
        var endpoint = _.apiUrl + action;

        Ti.API.info(endpoint);

        var h = Ti.Network.createHTTPClient({
            //timeout: 5000,
            cache: false,
            url: endpoint,
            onload: function(e){
                var data = JSON.parse( this.responseText );
                Ti.API.debug( data );
                if( callback ){
                    callback( data );
                }
            },
            onerror: function(e)
            {
                var data = JSON.parse( this.responseText );
                Ti.API.debug( e );
                if( failure_callback ){
                    failure_callback( data )
                }
            }
        });
        h.open( method , endpoint );

        var dm = require('/dbManager');
        var user = dm.getUser();
        if( user ){            
            h.setRequestHeader( 'X-AUTH-TOKEN' , user.apitoken );
        }
        h.send( params );
    },
};
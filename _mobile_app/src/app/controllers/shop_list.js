$.ai.show();

Alloy.m.apiSend('shop_list' , null , 'GET' , function(res){
	if( res.status ){
		var list = [];
		var margin = Ti.UI.createView({
			height: 12,
		});
		for(var i in res.data){
			var view = Ti.UI.createView({
				layout: 'vertical',
				width: '90%',
				height: Ti.UI.SIZE,
			});
			view.add(margin);
			var title = Ti.UI.createLabel({
				text:	res.data[i].name,
				font: {
					fontWeight: 'bold',
					fontSize: 18,
				},
				color: '#000000',
				textAlign: 'left',
				width: '100%',
			});
			view.add(title);

			var address = Ti.UI.createLabel({
				top: 2,
				font: {
					fontSize: 13,
				},
				color: '#666',
				width: '100%',
				textAlign: 'left',
				text: res.data[i].address + ', ' + res.data[i].zip + " " + res.data[i].city,
			});
			view.add(address);

			var conversion = Ti.UI.createLabel({
				top: 2,
				font: {
					fontSize: 13,
				},
				color: '#000',
				width: '100%',
				textAlign: 'left',
				text: 'Ponderazione: ' + res.data[i].conversion + ' ogni euro',
			});
			view.add(conversion);
			view.add(margin);
			
			var row = Ti.UI.createTableViewRow({
				height: Ti.UI.SIZE,
			});
			row.add(view);
			list.push(row);
		}
		$.table.data = list;
		$.ai.hide();
		$.table.visible = true;
	}else{
		alert(res.message);
	}
});
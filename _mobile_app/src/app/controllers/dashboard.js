function openBuy()
{
	var win = Alloy.createController('/buy',{ _nav: $.index }).getView();
	$.index.openWindow( win );
}
function openShopList()
{
	var win = Alloy.createController('/shop_list').getView();
	$.index.openWindow( win );
}
function openProfile()
{
	var win = Alloy.createController('/profile').getView();
	$.index.openWindow( win );
}
function getBalance()
{
	Alloy.m.apiSend('get_card_balance/' + user.card , null , 'GET' , function(res){
		if( res.status ){
			Alloy.m.balance = res.data.balance;
			updateBalance();
		}else{
			alert(res.message);
		}
	});
}
function updateBalance()
{
	$.balance.text = Alloy.m.balance;
}
function Logout()
{
	var ad = Ti.UI.createAlertDialog({
		title: 'Confermi uscita?',
		buttonNames: ['Annulla','Conferma'],
		cancel: 0,
	});
	ad.addEventListener('click', function(e){
		if( e.index==1 ){
			dm.Logout();
			var win = Alloy.createController('/signin').getView();
			$.index.openWindow( win );
		}
	});
	ad.show();
}

var dm = require('/dbManager');
var user = dm.getUser();
getBalance();
$.welcome.text = ( user.gender=='f' ? 'Benvenuta' : 'Benvenuto' ) + " " + user.firstname;

$.index.open();

Ti.App.addEventListener('updateBalance' , updateBalance);
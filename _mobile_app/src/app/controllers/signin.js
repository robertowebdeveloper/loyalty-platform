function Signin()
{
	var params = {
		email: $.email.value,
		password: $.password.value,
	};
	Alloy.m.apiSend( 'signin' , params , 'POST' , function(res){
		if( res.status ){
			var dm = require('/dbManager');
			dm.setModel( 'user' , {
				user_id: res.data.id,
				email: res.data.email,
				mobile: res.data.mobile,
				firstname: res.data.firstname,
				lastname: res.data.lastname,
				gender: res.data.gender,
				address: res.data.address,
				city: res.data.city,
				zip: res.data.zip,
				birthday: res.data.birthday,
				apitoken: res.data.apitoken,
				card: res.data.card,
			});
			
			var win = Alloy.createController('/dashboard').getView();
			win.open();

		}else{
			alert( res.message );
		}
	});
}
function openSignup()
{
	var win = Alloy.createController('/signup').getView();
	$.index.openWindow( win );
}

$.index.open();
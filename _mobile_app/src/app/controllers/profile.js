var gender = null;
var birthday = null;

function Save()
{
	var params = {
		email: user.email,
		mobile: $.mobile.value,
		firstname: $.firstname.value,
		lastname: $.lastname.value,
		gender: gender,
		address: $.address.value,
		city: $.city.value,
		zip: $.zip.value,
		birthday: birthday,
	};
	if( $.password.value.length>0 ){
		params.password = $.password.value;
	}
	Alloy.m.apiSend( 'update_customer' , params , 'PATCH' , function(res){
		dm.setModel( 'user' , {
			user_id: user.user_id,
			apitoken: user.apitoken,
			card: user.card,
			email: params.email,

			mobile: params.mobile,
			firstname: params.firstname,
			lastname: params.lastname,
			gender: params.gender,
			address: params.address,
			city: params.city,
			zip: params.zip,
			birthday: params.birthday,
		});
		var ad = Ti.UI.createAlertDialog({
			title: 'Dati salvati',
			message: ''
		});
		ad.show();
	}, function(res){		
		if(res.errors.length>0){
			var msg = '';
			for( var e in res.errors ){
				msg += '- ' + res.errors[e] + "\n";
			}
			alert(msg);
		}else{
			alert(res.message);
		}
	});
}

function openGender()
{
	var genderDialog = Ti.UI.createOptionDialog({
		title: 'Sesso',
		options: ['Uomo','Donna','Annulla'],
		cancel: 2,
	});
	genderDialog.addEventListener('click', function(e){
		if( e.index==0 ){
			gender = 'm';
			$.genderBox.text = 'Uomo';
		}else if( e.index==1 ){
			gender = 'f';
			$.genderBox.text = 'Donna';
		}
	});
	genderDialog.show();
}
function openBirthday()
{
	$.birthdayPickerBox.left = 0;
	$.birthdayPickerBox.visible = true;
	$.pickerBirthday.show();
}
function closeBirthday()
{
	$.birthdayPickerBox.left = -9999;
	$.birthdayPickerBox.visible = false;
	$.pickerBirthday.hide();
}
function setBirthday()
{
	var v = $.pickerBirthday.value;
	var y = v.getFullYear();
	var m = v.getMonth()+1;
	m = m<10 ? ('0' + m) : m;
	var d = v.getDate();
	d = d<10 ? ('0' + d) : d;
	
	birthday =  y + '-' + m + '-' + d;
	$.birthdayBox.text =  d + '-' + m + '-' + y;
	closeBirthday();
}

var dm = require('/dbManager');
var user = dm.getUser();
gender = user.gender;
if( gender=='f' ){
	$.genderBox.text = 'Donna';
}else if( gender=='m' ){
	$.genderBox.text = 'Uomo';
}else{
	$.genderBox.text = '-';
}

birthday = user.birthday;
if( birthday.length>0 ){
	var y = birthday.substring(0,4);
	var m = birthday.substring(5,7);
	var d = birthday.substring(8,10);
	$.birthdayBox.text = d + '-' + m + '-' + y;
}

$.item.fetch({
	user_id: user.user_id,
});
var gender = null;
var birthday = null;

function Signup()
{
	if(! $.privacy.value ){
		alert('Privacy non accettata');
	}else if(! $.terms.value ){
		alert('Termini e condizioni non accetati');
	}else{
		var params = {
			email: $.email.value,
			password: $.password.value,
			mobile: $.mobile.value,
			firstname: $.firstname.value,
			lastname: $.lastname.value,
			gender: gender,
			address: $.address.value,
			city: $.city.value,
			zip: $.zip.value,
			birthday: birthday,
		};
		Alloy.m.apiSend( 'signup' , params , 'POST' , function(res){
			if( res.status ){
				var ad = Ti.UI.createAlertDialog({
					title: 'Registrazione effettuata',
					message: 'Nella DEMO non è necessario confermare la mail. La tua utenza è subito attiva e puoi effettuare il login.'
				});
				ad.addEventListener('click',function(e){
					var win = Alloy.createController('/signin').getView();
					win.open();
				});
				ad.show();
				
			}else{
				var msg = '';
				for( var e in res['errors'] ){
					msg += '- ' + res['errors'][e] + "\n";
				}
				alert(msg);
			}
		});
	}
}

function openGender()
{
	var genderDialog = Ti.UI.createOptionDialog({
		title: 'Sesso',
		options: ['Uomo','Donna','Annulla'],
		cancel: 2,
	});
	genderDialog.addEventListener('click', function(e){
		if( e.index==0 ){
			gender = 'm';
			$.genderBox.text = 'Uomo';
		}else if( e.index==1 ){
			gender = 'f';
			$.genderBox.text = 'Donna';
		}
	});
	genderDialog.show();
}
function openBirthday()
{
	$.birthdayPickerBox.left = 0;
	$.birthdayPickerBox.visible = true;
	$.pickerBirthday.show();
}
function closeBirthday()
{
	$.birthdayPickerBox.left = -9999;
	$.birthdayPickerBox.visible = false;
	$.pickerBirthday.hide();
}
function setBirthday()
{
	var v = $.pickerBirthday.value;
	var y = v.getFullYear();
	var m = v.getMonth()+1;
	m = m<10 ? ('0' + m) : m;
	var d = v.getDate();
	d = d<10 ? ('0' + d) : d;
	
	birthday =  y + '-' + m + '-' + d;
	$.birthdayBox.text =  d + '-' + m + '-' + y;
	closeBirthday();
}
function Buy()
{
	var spent = $.spent.value;
	if(! spent>0 ){
		alert('L\'importo speso deve essere maggiore di 0');
	}else{
		var user = dm.getUser();
		Alloy.m.apiSend('add_movement', {
			cardCode:	user.card,
			shopId:		Alloy.m.demoShopId, //Nella demo 
			spent:		spent,
			type:		10,
			subtype:	10,
		}, 'POST' , function(res){
			if( res.status ){
				Alloy.m.balance = res.data.cardBalance;
				Ti.App.fireEvent('updateBalance');

				//Verifico se c'è da scalare anche un credito
				var cashback = $.cashback.value;
				if( cashback>0 ){
					Alloy.m.apiSend('add_movement', {
						cardCode:	user.card,
						shopId:		Alloy.m.demoShopId, //Nella demo 
						spent:		cashback,
						type:		20,
						subtype:	10,
					}, 'POST' , function(res){
						if( res.status ){
							Alloy.m.balance = res.data.cardBalance;
							Ti.App.fireEvent('updateBalance');
							goBack();
						}else{
							alert(res.message);
						}
					});
				}else{
					goBack();				}
			}else{
				alert(res.message);
			}
		});
	}
	Ti.App.fireEvent('getBalance');
}

function goBack()
{
	alert('Spesa effettuata');
	args._nav.closeWindow( $.index );
}

//Ricevo parametri in apertura
var args = arguments[0] || {};
$.balance.text = Alloy.m.balance;

var dm = require('/dbManager');
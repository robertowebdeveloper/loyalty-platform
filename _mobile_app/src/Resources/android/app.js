/**
 * Alloy for Titanium by Appcelerator
 * This is generated code, DO NOT MODIFY - changes will be lost!
 * Copyright (c) 2012 by Appcelerator, Inc.
 */
var Alloy = require('/alloy'),
_ = Alloy._,
Backbone = Alloy.Backbone;

global.Alloy = Alloy;
global._ = _;
global.Backbone = Backbone;

Alloy.m = {
  mainDbName: 'loyaltyplatform',
  //per sviluppo locale: apiUrl: 'http://loyalty.local:8888/api/',
  apiUrl: 'http://loyalty.jmcsoft.it/api/',

  demoShopId: 1, //ID del negozio usato nella demo,

  apiSend: function (action, params, method, callback)
  {
    var _ = this;
    var endpoint = _.apiUrl + action;

    Ti.API.info(endpoint);

    var h = Ti.Network.createHTTPClient({
      //timeout: 5000,
      cache: false,
      url: endpoint,
      onload: function (e) {
        var data = JSON.parse(this.responseText);
        Ti.API.debug(data);
        if (callback) {
          callback(data);
        }
      },
      onerror: function (e)
      {
        Ti.API.debug(e);
      } });

    h.open(method, endpoint);

    var dm = require('/dbManager');
    var user = dm.getUser();
    if (user) {
      h.setRequestHeader('X-AUTH-TOKEN', user.apitoken);
    }
    h.send(params);
  } };


Alloy.createController('index');
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/app.js.map
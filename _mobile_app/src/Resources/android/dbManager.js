/*
* Apre connessione database e torna manager
*/
exports.open = function ()
{
  return Ti.Database.open(Alloy.m.mainDbName);
};

/*
   * ritorna il tempo attuale in formato timestamp
   */
exports.now = function ()
{
  return parseInt(Math.floor(new Date().getTime() / 1000));
};

/**
    * Ritorna l'utente loggato (se loggato) altrimenti false
    */
exports.getUser = function () {
  var db = this.open();
  if (!db) {
    return false;
  }
  var ris = db.execute("SELECT * FROM `user` WHERE 1 LIMIT 1");

  var item = false;
  if (ris) {
    while (ris.isValidRow()) {
      item = {};
      item.user_id = ris.fieldByName('user_id');
      item.firstname = ris.fieldByName('firstname');
      item.lastname = ris.fieldByName('lastname');
      item.gender = ris.fieldByName('gender');
      item.birthday = ris.fieldByName('birthday');
      item.mobile = ris.fieldByName('mobile');
      item.email = ris.fieldByName('email');
      item.balance = ris.fieldByName('balance');
      item.city = ris.fieldByName('city');
      item.address = ris.fieldByName('address');
      item.zip = ris.fieldByName('zip');
      item.apitoken = ris.fieldByName('apitoken');
      item.card = ris.fieldByName('card');
      ris.next();
    }
    ris.close();
  }
  db.close();

  return item;
};

exports.setModel = function (entity, data)
{
  var model = Alloy.createModel(entity);
  model.set(data);
  model.save();
};

exports.Logout = function () {//Elimina i dati da tutte le tabelle
  var db = Ti.Database.open(Alloy.m.mainDbName);
  db.execute("DELETE FROM user");
  db.close();
};
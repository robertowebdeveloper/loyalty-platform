var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'signup';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};

  // Generated code that must be executed before all UI and/or
  // controller code. One example is all model and collection
  // declarations from markup.


  // Generated UI code
  $.__views["signup"] = Ti.UI.createWindow(
  { backgroundColor: "#fff", title: "Registrazione", backButtonTitle: "", id: "signup" });

  $.__views["signup"] && $.addTopLevelView($.__views["signup"]);
  $.__views["__alloyId67"] = Ti.UI.createScrollView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "__alloyId67" });

  $.__views["signup"].add($.__views["__alloyId67"]);
  $.__views["__alloyId68"] = Ti.UI.createView(
  { height: 10, id: "__alloyId68" });

  $.__views["__alloyId67"].add($.__views["__alloyId68"]);
  $.__views["__alloyId69"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", padding: { top: 8, right: 8, bottom: 8, left: 8 }, text: 'Compila i seguenti dati per registrati', id: "__alloyId69" });

  $.__views["__alloyId67"].add($.__views["__alloyId69"]);
  $.__views["__alloyId70"] = Ti.UI.createView(
  { top: 10, bottom: 10, width: "90%", height: 1, backgroundColor: "#444", id: "__alloyId70" });

  $.__views["__alloyId67"].add($.__views["__alloyId70"]);
  $.__views["__alloyId71"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'E-mail', id: "__alloyId71" });

  $.__views["__alloyId67"].add($.__views["__alloyId71"]);
  $.__views["email"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "email", hintText: "E-mail" });

  $.__views["__alloyId67"].add($.__views["email"]);
  $.__views["__alloyId72"] = Ti.UI.createView(
  { height: 10, id: "__alloyId72" });

  $.__views["__alloyId67"].add($.__views["__alloyId72"]);
  $.__views["__alloyId73"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Password', id: "__alloyId73" });

  $.__views["__alloyId67"].add($.__views["__alloyId73"]);
  $.__views["password"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "password", hintText: "Password", passwordMask: true });

  $.__views["__alloyId67"].add($.__views["password"]);
  $.__views["__alloyId74"] = Ti.UI.createView(
  { height: 10, id: "__alloyId74" });

  $.__views["__alloyId67"].add($.__views["__alloyId74"]);
  $.__views["__alloyId75"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Nome', id: "__alloyId75" });

  $.__views["__alloyId67"].add($.__views["__alloyId75"]);
  $.__views["firstname"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "firstname", hintText: "Nome" });

  $.__views["__alloyId67"].add($.__views["firstname"]);
  $.__views["__alloyId76"] = Ti.UI.createView(
  { height: 10, id: "__alloyId76" });

  $.__views["__alloyId67"].add($.__views["__alloyId76"]);
  $.__views["__alloyId77"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Cognome', id: "__alloyId77" });

  $.__views["__alloyId67"].add($.__views["__alloyId77"]);
  $.__views["lastname"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "lastname", hintText: "Cognome" });

  $.__views["__alloyId67"].add($.__views["lastname"]);
  $.__views["__alloyId78"] = Ti.UI.createView(
  { height: 10, id: "__alloyId78" });

  $.__views["__alloyId67"].add($.__views["__alloyId78"]);
  $.__views["__alloyId79"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Sesso', id: "__alloyId79" });

  $.__views["__alloyId67"].add($.__views["__alloyId79"]);
  $.__views["genderBox"] = Ti.UI.createLabel(
  { width: "90%", height: 40, textAlign: "left", font: { fontSize: 14 }, color: "#dd0000", backgroundColor: "#f4f4f4", text: '-', id: "genderBox" });

  $.__views["__alloyId67"].add($.__views["genderBox"]);
  openGender ? $.addListener($.__views["genderBox"], 'click', openGender) : __defers['$.__views["genderBox"]!click!openGender'] = true;$.__views["__alloyId80"] = Ti.UI.createView(
  { height: 10, id: "__alloyId80" });

  $.__views["__alloyId67"].add($.__views["__alloyId80"]);
  $.__views["__alloyId81"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Data di nascita', id: "__alloyId81" });

  $.__views["__alloyId67"].add($.__views["__alloyId81"]);
  $.__views["birthdayBox"] = Ti.UI.createLabel(
  { width: "90%", height: 40, textAlign: "left", font: { fontSize: 14 }, color: "#dd0000", backgroundColor: "#f4f4f4", text: '-', id: "birthdayBox" });

  $.__views["__alloyId67"].add($.__views["birthdayBox"]);
  openBirthday ? $.addListener($.__views["birthdayBox"], 'click', openBirthday) : __defers['$.__views["birthdayBox"]!click!openBirthday'] = true;$.__views["__alloyId82"] = Ti.UI.createView(
  { height: 10, id: "__alloyId82" });

  $.__views["__alloyId67"].add($.__views["__alloyId82"]);
  $.__views["__alloyId83"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Cellulare', id: "__alloyId83" });

  $.__views["__alloyId67"].add($.__views["__alloyId83"]);
  $.__views["mobile"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "mobile", hintText: "Cellulare" });

  $.__views["__alloyId67"].add($.__views["mobile"]);
  $.__views["__alloyId84"] = Ti.UI.createView(
  { height: 10, id: "__alloyId84" });

  $.__views["__alloyId67"].add($.__views["__alloyId84"]);
  $.__views["__alloyId85"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Città', id: "__alloyId85" });

  $.__views["__alloyId67"].add($.__views["__alloyId85"]);
  $.__views["city"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "city", hintText: "Città" });

  $.__views["__alloyId67"].add($.__views["city"]);
  $.__views["__alloyId86"] = Ti.UI.createView(
  { height: 10, id: "__alloyId86" });

  $.__views["__alloyId67"].add($.__views["__alloyId86"]);
  $.__views["__alloyId87"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Indirizzo', id: "__alloyId87" });

  $.__views["__alloyId67"].add($.__views["__alloyId87"]);
  $.__views["address"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "address", hintText: "Indirizzo" });

  $.__views["__alloyId67"].add($.__views["address"]);
  $.__views["__alloyId88"] = Ti.UI.createView(
  { height: 10, id: "__alloyId88" });

  $.__views["__alloyId67"].add($.__views["__alloyId88"]);
  $.__views["__alloyId89"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'CAP', id: "__alloyId89" });

  $.__views["__alloyId67"].add($.__views["__alloyId89"]);
  $.__views["zip"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "zip", hintText: "CAP" });

  $.__views["__alloyId67"].add($.__views["zip"]);
  $.__views["__alloyId90"] = Ti.UI.createView(
  { height: 10, id: "__alloyId90" });

  $.__views["__alloyId67"].add($.__views["__alloyId90"]);
  $.__views["__alloyId91"] = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: "90%", id: "__alloyId91" });

  $.__views["__alloyId67"].add($.__views["__alloyId91"]);
  $.__views["__alloyId92"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Accetto termini privacy', id: "__alloyId92" });

  $.__views["__alloyId91"].add($.__views["__alloyId92"]);
  $.__views["privacy"] = Ti.UI.createSwitch(
  { id: "privacy", right: 0 });

  $.__views["__alloyId91"].add($.__views["privacy"]);
  $.__views["__alloyId93"] = Ti.UI.createView(
  { height: 10, id: "__alloyId93" });

  $.__views["__alloyId67"].add($.__views["__alloyId93"]);
  $.__views["__alloyId94"] = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: "90%", id: "__alloyId94" });

  $.__views["__alloyId67"].add($.__views["__alloyId94"]);
  $.__views["__alloyId95"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Accetto termini e condizioni', id: "__alloyId95" });

  $.__views["__alloyId94"].add($.__views["__alloyId95"]);
  $.__views["terms"] = Ti.UI.createSwitch(
  { id: "terms", right: 0 });

  $.__views["__alloyId94"].add($.__views["terms"]);
  $.__views["__alloyId96"] = Ti.UI.createView(
  { height: 10, id: "__alloyId96" });

  $.__views["__alloyId67"].add($.__views["__alloyId96"]);
  $.__views["__alloyId97"] = Ti.UI.createButton(
  { title: 'REGISTRATI', id: "__alloyId97" });

  $.__views["__alloyId67"].add($.__views["__alloyId97"]);
  Signup ? $.addListener($.__views["__alloyId97"], 'click', Signup) : __defers['$.__views["__alloyId97"]!click!Signup'] = true;$.__views["__alloyId98"] = Ti.UI.createView(
  { height: 10, id: "__alloyId98" });

  $.__views["__alloyId67"].add($.__views["__alloyId98"]);
  $.__views["birthdayPickerBox"] = Ti.UI.createView(
  { left: -9999, visible: false, layout: "vertical", bottom: 0, height: Ti.UI.SIZE, zIndex: 100, backgroundColor: "#333333", id: "birthdayPickerBox" });

  $.__views["signup"].add($.__views["birthdayPickerBox"]);
  $.__views["__alloyId99"] = Ti.UI.createView(
  { height: Ti.UI.SIZE, backgroundColor: "#ccc", id: "__alloyId99" });

  $.__views["birthdayPickerBox"].add($.__views["__alloyId99"]);
  $.__views["__alloyId100"] = Ti.UI.createButton(
  { title: 'Annulla', left: 0, width: "49%", textAlign: "center", id: "__alloyId100" });

  $.__views["__alloyId99"].add($.__views["__alloyId100"]);
  closeBirthday ? $.addListener($.__views["__alloyId100"], 'click', closeBirthday) : __defers['$.__views["__alloyId100"]!click!closeBirthday'] = true;$.__views["__alloyId101"] = Ti.UI.createButton(
  { title: 'Conferma', right: 0, width: "49%", textAlign: "center", id: "__alloyId101" });

  $.__views["__alloyId99"].add($.__views["__alloyId101"]);
  setBirthday ? $.addListener($.__views["__alloyId101"], 'click', setBirthday) : __defers['$.__views["__alloyId101"]!click!setBirthday'] = true;$.__views["pickerBirthday"] = Ti.UI.createPicker(
  { minDate: new Date("Wed Jan 01 1930 00:00:00 GMT+0100 (GMT+01:00)"), format24: false, calendarViewShown: false, id: "pickerBirthday", type: Ti.UI.PICKER_TYPE_DATE });

  $.__views["birthdayPickerBox"].add($.__views["pickerBirthday"]);
  exports.destroy = function () {};

  // make all IDed elements in $.__views available right on the $ in a
  // controller's internal code. Externally the IDed elements will
  // be accessed with getView().
  _.extend($, $.__views);

  // Controller code directly from the developer's controller file
  var gender = null;
  var birthday = null;

  function Signup()
  {
    if (!$.privacy.value) {
      alert('Privacy non accettata');
    } else if (!$.terms.value) {
      alert('Termini e condizioni non accetati');
    } else {
      var params = {
        email: $.email.value,
        password: $.password.value,
        mobile: $.mobile.value,
        firstname: $.firstname.value,
        lastname: $.lastname.value,
        gender: gender,
        address: $.address.value,
        city: $.city.value,
        zip: $.zip.value,
        birthday: birthday };

      Alloy.m.apiSend('signup', params, 'POST', function (res) {
        if (res.status) {
          var ad = Ti.UI.createAlertDialog({
            title: 'Registrazione effettuata',
            message: 'Nella DEMO non è necessario confermare la mail. La tua utenza è subito attiva e puoi effettuare il login.' });

          ad.addEventListener('click', function (e) {
            var win = Alloy.createController('/signin').getView();
            win.open();
          });
          ad.show();

        } else {
          var msg = '';
          for (var e in res['errors']) {
            msg += '- ' + res['errors'][e] + "\n";
          }
          alert(msg);
        }
      });
    }
  }

  function openGender()
  {
    var genderDialog = Ti.UI.createOptionDialog({
      title: 'Sesso',
      options: ['Uomo', 'Donna', 'Annulla'],
      cancel: 2 });

    genderDialog.addEventListener('click', function (e) {
      if (e.index == 0) {
        gender = 'm';
        $.genderBox.text = 'Uomo';
      } else if (e.index == 1) {
        gender = 'f';
        $.genderBox.text = 'Donna';
      }
    });
    genderDialog.show();
  }
  function openBirthday()
  {
    $.birthdayPickerBox.left = 0;
    $.birthdayPickerBox.visible = true;
    $.pickerBirthday.show();
  }
  function closeBirthday()
  {
    $.birthdayPickerBox.left = -9999;
    $.birthdayPickerBox.visible = false;
    $.pickerBirthday.hide();
  }
  function setBirthday()
  {
    var v = $.pickerBirthday.value;
    var y = v.getFullYear();
    var m = v.getMonth() + 1;
    m = m < 10 ? '0' + m : m;
    var d = v.getDate();
    d = d < 10 ? '0' + d : d;

    birthday = y + '-' + m + '-' + d;
    $.birthdayBox.text = d + '-' + m + '-' + y;
    closeBirthday();
  }

  // Generated code that must be executed after all UI and
  // controller code. One example deferred event handlers whose
  // functions are not defined until after the controller code
  // is executed.
  __defers['$.__views["genderBox"]!click!openGender'] && $.addListener($.__views["genderBox"], 'click', openGender);__defers['$.__views["birthdayBox"]!click!openBirthday'] && $.addListener($.__views["birthdayBox"], 'click', openBirthday);__defers['$.__views["__alloyId97"]!click!Signup'] && $.addListener($.__views["__alloyId97"], 'click', Signup);__defers['$.__views["__alloyId100"]!click!closeBirthday'] && $.addListener($.__views["__alloyId100"], 'click', closeBirthday);__defers['$.__views["__alloyId101"]!click!setBirthday'] && $.addListener($.__views["__alloyId101"], 'click', setBirthday);

  // Extend the $ instance with all functions and properties
  // defined on the exports object.
  _.extend($, exports);
}

module.exports = Controller;
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/alloy/controllers/signup.js.map
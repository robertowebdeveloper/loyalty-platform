var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'profile';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};

  // Generated code that must be executed before all UI and/or
  // controller code. One example is all model and collection
  // declarations from markup.
  $.item = Alloy.createModel('user');

  // Generated UI code
  $.__views["profile"] = Ti.UI.createWindow(
  { backgroundColor: "#fff", title: "Il tuo profilo", backButtonTitle: "", id: "profile" });

  $.__views["profile"] && $.addTopLevelView($.__views["profile"]);
  $.__views["__alloyId29"] = Ti.UI.createScrollView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "__alloyId29" });

  $.__views["profile"].add($.__views["__alloyId29"]);
  $.__views["__alloyId30"] = Ti.UI.createView(
  { height: 10, id: "__alloyId30" });

  $.__views["__alloyId29"].add($.__views["__alloyId30"]);
  $.__views["__alloyId31"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'E-mail', id: "__alloyId31" });

  $.__views["__alloyId29"].add($.__views["__alloyId31"]);
  $.__views["__alloyId32"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", id: "__alloyId32" });

  $.__views["__alloyId29"].add($.__views["__alloyId32"]);
  $.__views["__alloyId33"] = Ti.UI.createView(
  { height: 10, id: "__alloyId33" });

  $.__views["__alloyId29"].add($.__views["__alloyId33"]);
  $.__views["__alloyId34"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Password', id: "__alloyId34" });

  $.__views["__alloyId29"].add($.__views["__alloyId34"]);
  $.__views["password"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "password", hintText: "Password", passwordMask: true });

  $.__views["__alloyId29"].add($.__views["password"]);
  $.__views["__alloyId35"] = Ti.UI.createView(
  { height: 10, id: "__alloyId35" });

  $.__views["__alloyId29"].add($.__views["__alloyId35"]);
  $.__views["__alloyId36"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Nome', id: "__alloyId36" });

  $.__views["__alloyId29"].add($.__views["__alloyId36"]);
  $.__views["firstname"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "firstname", hintText: "Nome" });

  $.__views["__alloyId29"].add($.__views["firstname"]);
  $.__views["__alloyId37"] = Ti.UI.createView(
  { height: 10, id: "__alloyId37" });

  $.__views["__alloyId29"].add($.__views["__alloyId37"]);
  $.__views["__alloyId38"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Cognome', id: "__alloyId38" });

  $.__views["__alloyId29"].add($.__views["__alloyId38"]);
  $.__views["lastname"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "lastname", hintText: "Cognome" });

  $.__views["__alloyId29"].add($.__views["lastname"]);
  $.__views["__alloyId39"] = Ti.UI.createView(
  { height: 10, id: "__alloyId39" });

  $.__views["__alloyId29"].add($.__views["__alloyId39"]);
  $.__views["__alloyId40"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Sesso', id: "__alloyId40" });

  $.__views["__alloyId29"].add($.__views["__alloyId40"]);
  $.__views["genderBox"] = Ti.UI.createLabel(
  { width: "90%", height: 40, textAlign: "left", font: { fontSize: 14 }, color: "#dd0000", backgroundColor: "#f4f4f4", text: '-', id: "genderBox" });

  $.__views["__alloyId29"].add($.__views["genderBox"]);
  openGender ? $.addListener($.__views["genderBox"], 'click', openGender) : __defers['$.__views["genderBox"]!click!openGender'] = true;$.__views["__alloyId41"] = Ti.UI.createView(
  { height: 10, id: "__alloyId41" });

  $.__views["__alloyId29"].add($.__views["__alloyId41"]);
  $.__views["__alloyId42"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Data di nascita', id: "__alloyId42" });

  $.__views["__alloyId29"].add($.__views["__alloyId42"]);
  $.__views["birthdayBox"] = Ti.UI.createLabel(
  { width: "90%", height: 40, textAlign: "left", font: { fontSize: 14 }, color: "#dd0000", backgroundColor: "#f4f4f4", text: '-', id: "birthdayBox" });

  $.__views["__alloyId29"].add($.__views["birthdayBox"]);
  openBirthday ? $.addListener($.__views["birthdayBox"], 'click', openBirthday) : __defers['$.__views["birthdayBox"]!click!openBirthday'] = true;$.__views["__alloyId43"] = Ti.UI.createView(
  { height: 10, id: "__alloyId43" });

  $.__views["__alloyId29"].add($.__views["__alloyId43"]);
  $.__views["__alloyId44"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Cellulare', id: "__alloyId44" });

  $.__views["__alloyId29"].add($.__views["__alloyId44"]);
  $.__views["mobile"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "mobile", hintText: "Cellulare" });

  $.__views["__alloyId29"].add($.__views["mobile"]);
  $.__views["__alloyId45"] = Ti.UI.createView(
  { height: 10, id: "__alloyId45" });

  $.__views["__alloyId29"].add($.__views["__alloyId45"]);
  $.__views["__alloyId46"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Città', id: "__alloyId46" });

  $.__views["__alloyId29"].add($.__views["__alloyId46"]);
  $.__views["city"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "city", hintText: "Città" });

  $.__views["__alloyId29"].add($.__views["city"]);
  $.__views["__alloyId47"] = Ti.UI.createView(
  { height: 10, id: "__alloyId47" });

  $.__views["__alloyId29"].add($.__views["__alloyId47"]);
  $.__views["__alloyId48"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'Indirizzo', id: "__alloyId48" });

  $.__views["__alloyId29"].add($.__views["__alloyId48"]);
  $.__views["address"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "address", hintText: "Indirizzo" });

  $.__views["__alloyId29"].add($.__views["address"]);
  $.__views["__alloyId49"] = Ti.UI.createView(
  { height: 10, id: "__alloyId49" });

  $.__views["__alloyId29"].add($.__views["__alloyId49"]);
  $.__views["__alloyId50"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: 'CAP', id: "__alloyId50" });

  $.__views["__alloyId29"].add($.__views["__alloyId50"]);
  $.__views["zip"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "zip", hintText: "CAP" });

  $.__views["__alloyId29"].add($.__views["zip"]);
  $.__views["__alloyId51"] = Ti.UI.createView(
  { height: 10, id: "__alloyId51" });

  $.__views["__alloyId29"].add($.__views["__alloyId51"]);
  $.__views["__alloyId52"] = Ti.UI.createButton(
  { title: 'SALVA', id: "__alloyId52" });

  $.__views["__alloyId29"].add($.__views["__alloyId52"]);
  Save ? $.addListener($.__views["__alloyId52"], 'click', Save) : __defers['$.__views["__alloyId52"]!click!Save'] = true;$.__views["__alloyId53"] = Ti.UI.createView(
  { height: 10, id: "__alloyId53" });

  $.__views["__alloyId29"].add($.__views["__alloyId53"]);
  $.__views["birthdayPickerBox"] = Ti.UI.createView(
  { left: -9999, visible: false, layout: "vertical", bottom: 0, height: Ti.UI.SIZE, zIndex: 100, backgroundColor: "#333333", id: "birthdayPickerBox" });

  $.__views["profile"].add($.__views["birthdayPickerBox"]);
  $.__views["__alloyId54"] = Ti.UI.createView(
  { height: Ti.UI.SIZE, backgroundColor: "#ccc", id: "__alloyId54" });

  $.__views["birthdayPickerBox"].add($.__views["__alloyId54"]);
  $.__views["__alloyId55"] = Ti.UI.createButton(
  { title: 'Annulla', left: 0, width: "49%", textAlign: "center", id: "__alloyId55" });

  $.__views["__alloyId54"].add($.__views["__alloyId55"]);
  closeBirthday ? $.addListener($.__views["__alloyId55"], 'click', closeBirthday) : __defers['$.__views["__alloyId55"]!click!closeBirthday'] = true;$.__views["__alloyId56"] = Ti.UI.createButton(
  { title: 'Conferma', right: 0, width: "49%", textAlign: "center", id: "__alloyId56" });

  $.__views["__alloyId54"].add($.__views["__alloyId56"]);
  setBirthday ? $.addListener($.__views["__alloyId56"], 'click', setBirthday) : __defers['$.__views["__alloyId56"]!click!setBirthday'] = true;$.__views["pickerBirthday"] = Ti.UI.createPicker(
  { minDate: new Date("Wed Jan 01 1930 00:00:00 GMT+0100 (GMT+01:00)"), format24: false, calendarViewShown: false, id: "pickerBirthday", type: Ti.UI.PICKER_TYPE_DATE });

  $.__views["birthdayPickerBox"].add($.__views["pickerBirthday"]);
  var __alloyId57 = function () {$['item'].__transform = _.isFunction($['item'].transform) ? $['item'].transform() : $['item'].toJSON();$.__alloyId32.text = $['item']['__transform']['email'];$.firstname.value = $['item']['__transform']['firstname'];$.lastname.value = $['item']['__transform']['lastname'];$.mobile.value = $['item']['__transform']['mobile'];$.city.value = $['item']['__transform']['city'];$.address.value = $['item']['__transform']['address'];$.zip.value = $['item']['__transform']['zip'];};$['item'].on('fetch change destroy', __alloyId57);exports.destroy = function () {$['item'] && $['item'].off('fetch change destroy', __alloyId57);};

  // make all IDed elements in $.__views available right on the $ in a
  // controller's internal code. Externally the IDed elements will
  // be accessed with getView().
  _.extend($, $.__views);

  // Controller code directly from the developer's controller file
  var gender = null;
  var birthday = null;

  function Save()
  {
    var params = {
      email: user.email,
      mobile: $.mobile.value,
      firstname: $.firstname.value,
      lastname: $.lastname.value,
      gender: gender,
      address: $.address.value,
      city: $.city.value,
      zip: $.zip.value,
      birthday: birthday };

    if ($.password.value.length > 0) {
      params.password = $.password.value;
    }
    Alloy.m.apiSend('update_customer', params, 'PATCH', function (res) {
      if (res.status) {
        dm.setModel('user', {
          user_id: user.user_id,
          apitoken: user.apitoken,
          card: user.card,
          email: params.email,

          mobile: params.mobile,
          firstname: params.firstname,
          lastname: params.lastname,
          gender: params.gender,
          address: params.address,
          city: params.city,
          zip: params.zip,
          birthday: params.birthday });

        var ad = Ti.UI.createAlertDialog({
          title: 'Dati salvati',
          message: '' });

        ad.show();

      } else if (res.errors.length > 0) {
        var msg = '';
        for (var e in res.errors) {
          msg += '- ' + res.errors[e] + "\n";
        }
        alert(msg);
      } else {
        alert(res.message);
      }
    });
  }

  function openGender()
  {
    var genderDialog = Ti.UI.createOptionDialog({
      title: 'Sesso',
      options: ['Uomo', 'Donna', 'Annulla'],
      cancel: 2 });

    genderDialog.addEventListener('click', function (e) {
      if (e.index == 0) {
        gender = 'm';
        $.genderBox.text = 'Uomo';
      } else if (e.index == 1) {
        gender = 'f';
        $.genderBox.text = 'Donna';
      }
    });
    genderDialog.show();
  }
  function openBirthday()
  {
    $.birthdayPickerBox.left = 0;
    $.birthdayPickerBox.visible = true;
    $.pickerBirthday.show();
  }
  function closeBirthday()
  {
    $.birthdayPickerBox.left = -9999;
    $.birthdayPickerBox.visible = false;
    $.pickerBirthday.hide();
  }
  function setBirthday()
  {
    var v = $.pickerBirthday.value;
    var y = v.getFullYear();
    var m = v.getMonth() + 1;
    m = m < 10 ? '0' + m : m;
    var d = v.getDate();
    d = d < 10 ? '0' + d : d;

    birthday = y + '-' + m + '-' + d;
    $.birthdayBox.text = d + '-' + m + '-' + y;
    closeBirthday();
  }

  var dm = require('/dbManager');
  var user = dm.getUser();
  gender = user.gender;
  if (gender == 'f') {
    $.genderBox.text = 'Donna';
  } else if (gender == 'm') {
    $.genderBox.text = 'Uomo';
  } else {
    $.genderBox.text = '-';
  }

  birthday = user.birthday;
  if (birthday.length > 0) {
    var y = birthday.substring(0, 4);
    var m = birthday.substring(5, 7);
    var d = birthday.substring(8, 10);
    $.birthdayBox.text = d + '-' + m + '-' + y;
  }

  $.item.fetch({
    user_id: user.user_id });

  // Generated code that must be executed after all UI and
  // controller code. One example deferred event handlers whose
  // functions are not defined until after the controller code
  // is executed.
  __defers['$.__views["genderBox"]!click!openGender'] && $.addListener($.__views["genderBox"], 'click', openGender);__defers['$.__views["birthdayBox"]!click!openBirthday'] && $.addListener($.__views["birthdayBox"], 'click', openBirthday);__defers['$.__views["__alloyId52"]!click!Save'] && $.addListener($.__views["__alloyId52"], 'click', Save);__defers['$.__views["__alloyId55"]!click!closeBirthday'] && $.addListener($.__views["__alloyId55"], 'click', closeBirthday);__defers['$.__views["__alloyId56"]!click!setBirthday'] && $.addListener($.__views["__alloyId56"], 'click', setBirthday);

  // Extend the $ instance with all functions and properties
  // defined on the exports object.
  _.extend($, exports);
}

module.exports = Controller;
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/alloy/controllers/profile.js.map
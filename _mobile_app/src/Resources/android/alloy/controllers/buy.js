var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'buy';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};

  // Generated code that must be executed before all UI and/or
  // controller code. One example is all model and collection
  // declarations from markup.


  // Generated UI code
  $.__views["index"] = Ti.UI.createWindow(
  { backgroundColor: "#fff", id: "index", title: "Esegui acquisto", backButtonTitle: "" });

  $.__views["index"] && $.addTopLevelView($.__views["index"]);
  $.__views["__alloyId0"] = Ti.UI.createScrollView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "__alloyId0" });

  $.__views["index"].add($.__views["__alloyId0"]);
  $.__views["__alloyId1"] = Ti.UI.createView(
  { height: 10, id: "__alloyId1" });

  $.__views["__alloyId0"].add($.__views["__alloyId1"]);
  $.__views["__alloyId2"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14, fontWeight: "bold" }, color: "#000", text: 'IL TUO SALDO', id: "__alloyId2" });

  $.__views["__alloyId0"].add($.__views["__alloyId2"]);
  $.__views["balance"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", text: '-', id: "balance" });

  $.__views["__alloyId0"].add($.__views["balance"]);
  $.__views["__alloyId3"] = Ti.UI.createView(
  { height: 10, id: "__alloyId3" });

  $.__views["__alloyId0"].add($.__views["__alloyId3"]);
  $.__views["__alloyId4"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", padding: { top: 8, right: 8, bottom: 8, left: 8 }, text: 'Inserisci l\'importo speso in euro', id: "__alloyId4" });

  $.__views["__alloyId0"].add($.__views["__alloyId4"]);
  $.__views["spent"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "spent", hintText: "Importo", keyboardType: Ti.UI.KEYBOARD_TYPE_DECIMAL_PAD });

  $.__views["__alloyId0"].add($.__views["spent"]);
  $.__views["__alloyId5"] = Ti.UI.createView(
  { height: 10, id: "__alloyId5" });

  $.__views["__alloyId0"].add($.__views["__alloyId5"]);
  $.__views["__alloyId6"] = Ti.UI.createView(
  { height: 10, id: "__alloyId6" });

  $.__views["__alloyId0"].add($.__views["__alloyId6"]);
  $.__views["__alloyId7"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", padding: { top: 8, right: 8, bottom: 8, left: 8 }, text: 'Inserisci il numero di punti da scalare (100 punti = 1 euro)', id: "__alloyId7" });

  $.__views["__alloyId0"].add($.__views["__alloyId7"]);
  $.__views["cashback"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "cashback", hintText: "Punti da usare", keyboardType: Ti.UI.KEYBOARD_TYPE_DECIMAL_PAD });

  $.__views["__alloyId0"].add($.__views["cashback"]);
  $.__views["__alloyId8"] = Ti.UI.createView(
  { height: 10, id: "__alloyId8" });

  $.__views["__alloyId0"].add($.__views["__alloyId8"]);
  $.__views["__alloyId9"] = Ti.UI.createView(
  { height: 10, id: "__alloyId9" });

  $.__views["__alloyId0"].add($.__views["__alloyId9"]);
  $.__views["__alloyId10"] = Ti.UI.createButton(
  { title: 'Esegui', id: "__alloyId10" });

  $.__views["__alloyId0"].add($.__views["__alloyId10"]);
  Buy ? $.addListener($.__views["__alloyId10"], 'click', Buy) : __defers['$.__views["__alloyId10"]!click!Buy'] = true;$.__views["__alloyId11"] = Ti.UI.createView(
  { height: 10, id: "__alloyId11" });

  $.__views["__alloyId0"].add($.__views["__alloyId11"]);
  $.__views["__alloyId12"] = Ti.UI.createView(
  { height: 10, id: "__alloyId12" });

  $.__views["__alloyId0"].add($.__views["__alloyId12"]);
  exports.destroy = function () {};

  // make all IDed elements in $.__views available right on the $ in a
  // controller's internal code. Externally the IDed elements will
  // be accessed with getView().
  _.extend($, $.__views);

  // Controller code directly from the developer's controller file
  function Buy()
  {
    var spent = $.spent.value;
    if (!spent > 0) {
      alert('L\'importo speso deve essere maggiore di 0');
    } else {
      var user = dm.getUser();
      Alloy.m.apiSend('add_movement', {
        cardCode: user.card,
        shopId: Alloy.m.demoShopId, //Nella demo 
        spent: spent,
        type: 10,
        subtype: 10 },
      'POST', function (res) {
        if (res.status) {
          Alloy.m.balance = res.data.cardBalance;
          Ti.App.fireEvent('updateBalance');

          //Verifico se c'è da scalare anche un credito
          var cashback = $.cashback.value;
          if (cashback > 0) {
            Alloy.m.apiSend('add_movement', {
              cardCode: user.card,
              shopId: Alloy.m.demoShopId, //Nella demo 
              spent: cashback,
              type: 20,
              subtype: 10 },
            'POST', function (res) {
              if (res.status) {
                Alloy.m.balance = res.data.cardBalance;
                Ti.App.fireEvent('updateBalance');
                goBack();
              } else {
                alert(res.message);
              }
            });
          } else {
            goBack();}
        } else {
          alert(res.message);
        }
      });
    }
    Ti.App.fireEvent('getBalance');
  }

  function goBack()
  {
    alert('Spesa effettuata');
    args._nav.closeWindow($.index);
  }

  //Ricevo parametri in apertura
  var args = arguments[0] || {};
  $.balance.text = Alloy.m.balance;

  var dm = require('/dbManager');

  // Generated code that must be executed after all UI and
  // controller code. One example deferred event handlers whose
  // functions are not defined until after the controller code
  // is executed.
  __defers['$.__views["__alloyId10"]!click!Buy'] && $.addListener($.__views["__alloyId10"], 'click', Buy);

  // Extend the $ instance with all functions and properties
  // defined on the exports object.
  _.extend($, exports);
}

module.exports = Controller;
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/alloy/controllers/buy.js.map
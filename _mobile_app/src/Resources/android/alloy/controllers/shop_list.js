var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'shop_list';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};

  // Generated code that must be executed before all UI and/or
  // controller code. One example is all model and collection
  // declarations from markup.


  // Generated UI code
  $.__views["index"] = Ti.UI.createWindow(
  { backgroundColor: "#fff", id: "index", title: "I nostri negozi", backButtonTitle: "" });

  $.__views["index"] && $.addTopLevelView($.__views["index"]);
  $.__views["ai"] = Ti.UI.createActivityIndicator(
  { style: Ti.UI.ActivityIndicatorStyle.BIG_DARK, id: "ai" });

  $.__views["index"].add($.__views["ai"]);
  $.__views["table"] = Ti.UI.createTableView(
  { visible: false, id: "table" });

  $.__views["index"].add($.__views["table"]);
  exports.destroy = function () {};

  // make all IDed elements in $.__views available right on the $ in a
  // controller's internal code. Externally the IDed elements will
  // be accessed with getView().
  _.extend($, $.__views);

  // Controller code directly from the developer's controller file
  $.ai.show();

  Alloy.m.apiSend('shop_list', null, 'GET', function (res) {
    if (res.status) {
      var list = [];
      var margin = Ti.UI.createView({
        height: 12 });

      for (var i in res.data) {
        var view = Ti.UI.createView({
          layout: 'vertical',
          width: '90%',
          height: Ti.UI.SIZE });

        view.add(margin);
        var title = Ti.UI.createLabel({
          text: res.data[i].name,
          font: {
            fontWeight: 'bold',
            fontSize: 18 },

          color: '#000000',
          textAlign: 'left',
          width: '100%' });

        view.add(title);

        var address = Ti.UI.createLabel({
          top: 2,
          font: {
            fontSize: 13 },

          color: '#666',
          width: '100%',
          textAlign: 'left',
          text: res.data[i].address + ', ' + res.data[i].zip + " " + res.data[i].city });

        view.add(address);

        var conversion = Ti.UI.createLabel({
          top: 2,
          font: {
            fontSize: 13 },

          color: '#000',
          width: '100%',
          textAlign: 'left',
          text: 'Ponderazione: ' + res.data[i].conversion + ' ogni euro' });

        view.add(conversion);
        view.add(margin);

        var row = Ti.UI.createTableViewRow({
          height: Ti.UI.SIZE });

        row.add(view);
        list.push(row);
      }
      $.table.data = list;
      $.ai.hide();
      $.table.visible = true;
    } else {
      alert(res.message);
    }
  });

  // Generated code that must be executed after all UI and
  // controller code. One example deferred event handlers whose
  // functions are not defined until after the controller code
  // is executed.


  // Extend the $ instance with all functions and properties
  // defined on the exports object.
  _.extend($, exports);
}

module.exports = Controller;
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/alloy/controllers/shop_list.js.map
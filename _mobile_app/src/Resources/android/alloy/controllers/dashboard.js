var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'dashboard';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};

  // Generated code that must be executed before all UI and/or
  // controller code. One example is all model and collection
  // declarations from markup.


  // Generated UI code
  $.__views["__alloyId13"] = Ti.UI.createWindow(
  { backgroundColor: "#fff", title: "Loyalty Platform", id: "__alloyId13" });

  $.__views["__alloyId14"] = Ti.UI.createScrollView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "__alloyId14" });

  $.__views["__alloyId13"].add($.__views["__alloyId14"]);
  $.__views["__alloyId15"] = Ti.UI.createView(
  { height: 10, id: "__alloyId15" });

  $.__views["__alloyId14"].add($.__views["__alloyId15"]);
  $.__views["__alloyId16"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "center", font: { fontSize: 24, fontWeight: "bold" }, color: "#000", text: 'Dashboard', id: "__alloyId16" });

  $.__views["__alloyId14"].add($.__views["__alloyId16"]);
  $.__views["__alloyId17"] = Ti.UI.createView(
  { height: 10, id: "__alloyId17" });

  $.__views["__alloyId14"].add($.__views["__alloyId17"]);
  $.__views["__alloyId18"] = Ti.UI.createImageView(
  { image: "/icon.png", width: 130, id: "__alloyId18" });

  $.__views["__alloyId14"].add($.__views["__alloyId18"]);
  $.__views["__alloyId19"] = Ti.UI.createView(
  { top: 10, bottom: 10, width: "90%", height: 1, backgroundColor: "#444", id: "__alloyId19" });

  $.__views["__alloyId14"].add($.__views["__alloyId19"]);
  $.__views["welcome"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14 }, color: "#000", id: "welcome" });

  $.__views["__alloyId14"].add($.__views["welcome"]);
  $.__views["__alloyId20"] = Ti.UI.createView(
  { height: 10, id: "__alloyId20" });

  $.__views["__alloyId14"].add($.__views["__alloyId20"]);
  $.__views["__alloyId21"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 14, fontWeight: "bold" }, color: "#000", text: 'IL TUO SALDO', id: "__alloyId21" });

  $.__views["__alloyId14"].add($.__views["__alloyId21"]);
  $.__views["balance"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "left", font: { fontSize: 20 }, color: "#dd0000", text: '-', id: "balance" });

  $.__views["__alloyId14"].add($.__views["balance"]);
  $.__views["__alloyId22"] = Ti.UI.createView(
  { height: 10, id: "__alloyId22" });

  $.__views["__alloyId14"].add($.__views["__alloyId22"]);
  $.__views["__alloyId23"] = Ti.UI.createButton(
  { backgroundColor: "#333", color: "#fff", borderRadius: 8, width: "90%", height: 50, font: { fontSize: 18 }, top: 12, title: 'Effettua acquisto', id: "__alloyId23" });

  $.__views["__alloyId14"].add($.__views["__alloyId23"]);
  openBuy ? $.addListener($.__views["__alloyId23"], 'click', openBuy) : __defers['$.__views["__alloyId23"]!click!openBuy'] = true;$.__views["__alloyId24"] = Ti.UI.createButton(
  { backgroundColor: "#333", color: "#fff", borderRadius: 8, width: "90%", height: 50, font: { fontSize: 18 }, top: 12, title: 'Elenco negozi', id: "__alloyId24" });

  $.__views["__alloyId14"].add($.__views["__alloyId24"]);
  openShopList ? $.addListener($.__views["__alloyId24"], 'click', openShopList) : __defers['$.__views["__alloyId24"]!click!openShopList'] = true;$.__views["__alloyId25"] = Ti.UI.createButton(
  { backgroundColor: "#333", color: "#fff", borderRadius: 8, width: "90%", height: 50, font: { fontSize: 18 }, top: 12, title: 'Modifica i tuoi dati', id: "__alloyId25" });

  $.__views["__alloyId14"].add($.__views["__alloyId25"]);
  openProfile ? $.addListener($.__views["__alloyId25"], 'click', openProfile) : __defers['$.__views["__alloyId25"]!click!openProfile'] = true;$.__views["__alloyId26"] = Ti.UI.createView(
  { height: 10, id: "__alloyId26" });

  $.__views["__alloyId14"].add($.__views["__alloyId26"]);
  $.__views["__alloyId27"] = Ti.UI.createView(
  { height: 10, id: "__alloyId27" });

  $.__views["__alloyId14"].add($.__views["__alloyId27"]);
  $.__views["__alloyId28"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "center", font: { fontSize: 14 }, color: "#000", text: 'Logout', id: "__alloyId28" });

  $.__views["__alloyId14"].add($.__views["__alloyId28"]);
  Logout ? $.addListener($.__views["__alloyId28"], 'click', Logout) : __defers['$.__views["__alloyId28"]!click!Logout'] = true;$.__views["index"] = Ti.UI.createNavigationWindow(
  { window: $.__views["__alloyId13"], id: "index", exitOnClose: true });

  $.__views["index"] && $.addTopLevelView($.__views["index"]);
  exports.destroy = function () {};

  // make all IDed elements in $.__views available right on the $ in a
  // controller's internal code. Externally the IDed elements will
  // be accessed with getView().
  _.extend($, $.__views);

  // Controller code directly from the developer's controller file
  function openBuy()
  {
    var win = Alloy.createController('/buy', { _nav: $.index }).getView();
    $.index.openWindow(win);
  }
  function openShopList()
  {
    var win = Alloy.createController('/shop_list').getView();
    $.index.openWindow(win);
  }
  function openProfile()
  {
    var win = Alloy.createController('/profile').getView();
    $.index.openWindow(win);
  }
  function getBalance()
  {
    Alloy.m.apiSend('get_card_balance/' + user.card, null, 'GET', function (res) {
      if (res.status) {
        Alloy.m.balance = res.data.balance;
        updateBalance();
      } else {
        alert(res.message);
      }
    });
  }
  function updateBalance()
  {
    $.balance.text = Alloy.m.balance;
  }
  function Logout()
  {
    var ad = Ti.UI.createAlertDialog({
      title: 'Confermi uscita?',
      buttonNames: ['Annulla', 'Conferma'],
      cancel: 0 });

    ad.addEventListener('click', function (e) {
      if (e.index == 1) {
        dm.Logout();
        var win = Alloy.createController('/signin').getView();
        $.index.openWindow(win);
      }
    });
    ad.show();
  }

  var dm = require('/dbManager');
  var user = dm.getUser();
  getBalance();
  $.welcome.text = (user.gender == 'f' ? 'Benvenuta' : 'Benvenuto') + " " + user.firstname;

  $.index.open();

  Ti.App.addEventListener('updateBalance', updateBalance);

  // Generated code that must be executed after all UI and
  // controller code. One example deferred event handlers whose
  // functions are not defined until after the controller code
  // is executed.
  __defers['$.__views["__alloyId23"]!click!openBuy'] && $.addListener($.__views["__alloyId23"], 'click', openBuy);__defers['$.__views["__alloyId24"]!click!openShopList'] && $.addListener($.__views["__alloyId24"], 'click', openShopList);__defers['$.__views["__alloyId25"]!click!openProfile'] && $.addListener($.__views["__alloyId25"], 'click', openProfile);__defers['$.__views["__alloyId28"]!click!Logout'] && $.addListener($.__views["__alloyId28"], 'click', Logout);

  // Extend the $ instance with all functions and properties
  // defined on the exports object.
  _.extend($, exports);
}

module.exports = Controller;
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/alloy/controllers/dashboard.js.map
var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'signin';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};

  // Generated code that must be executed before all UI and/or
  // controller code. One example is all model and collection
  // declarations from markup.


  // Generated UI code
  $.__views["__alloyId58"] = Ti.UI.createWindow(
  { backgroundColor: "#fff", title: "Loyalty Platform", id: "__alloyId58" });

  $.__views["__alloyId59"] = Ti.UI.createView(
  { layout: "vertical", height: Ti.UI.SIZE, id: "__alloyId59" });

  $.__views["__alloyId58"].add($.__views["__alloyId59"]);
  $.__views["email"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "email", hintText: "E-mail" });

  $.__views["__alloyId59"].add($.__views["email"]);
  $.__views["__alloyId60"] = Ti.UI.createView(
  { height: 10, id: "__alloyId60" });

  $.__views["__alloyId59"].add($.__views["__alloyId60"]);
  $.__views["password"] = Ti.UI.createTextField(
  { width: "90%", font: { fontSize: 16 }, textAlign: "left", backgroundColor: "#e4e4e4", padding: { top: 8, right: 8, bottom: 8, left: 8 }, height: Ti.UI.SIZE, color: "#000000", hintTextColor: "#666", id: "password", hintText: "Password", passwordMask: true });

  $.__views["__alloyId59"].add($.__views["password"]);
  $.__views["__alloyId61"] = Ti.UI.createView(
  { height: 10, id: "__alloyId61" });

  $.__views["__alloyId59"].add($.__views["__alloyId61"]);
  $.__views["__alloyId62"] = Ti.UI.createButton(
  { title: 'ACCEDI', id: "__alloyId62" });

  $.__views["__alloyId59"].add($.__views["__alloyId62"]);
  Signin ? $.addListener($.__views["__alloyId62"], 'click', Signin) : __defers['$.__views["__alloyId62"]!click!Signin'] = true;$.__views["__alloyId63"] = Ti.UI.createView(
  { height: 10, id: "__alloyId63" });

  $.__views["__alloyId59"].add($.__views["__alloyId63"]);
  $.__views["__alloyId64"] = Ti.UI.createView(
  { height: 10, id: "__alloyId64" });

  $.__views["__alloyId59"].add($.__views["__alloyId64"]);
  $.__views["__alloyId65"] = Ti.UI.createView(
  { height: 10, id: "__alloyId65" });

  $.__views["__alloyId59"].add($.__views["__alloyId65"]);
  $.__views["__alloyId66"] = Ti.UI.createLabel(
  { width: "90%", height: Ti.UI.SIZE, textAlign: "center", font: { fontSize: 14 }, color: "#000", text: 'Registrati', id: "__alloyId66" });

  $.__views["__alloyId59"].add($.__views["__alloyId66"]);
  openSignup ? $.addListener($.__views["__alloyId66"], 'click', openSignup) : __defers['$.__views["__alloyId66"]!click!openSignup'] = true;$.__views["index"] = Ti.UI.createNavigationWindow(
  { window: $.__views["__alloyId58"], id: "index" });

  $.__views["index"] && $.addTopLevelView($.__views["index"]);
  exports.destroy = function () {};

  // make all IDed elements in $.__views available right on the $ in a
  // controller's internal code. Externally the IDed elements will
  // be accessed with getView().
  _.extend($, $.__views);

  // Controller code directly from the developer's controller file
  function Signin()
  {
    var params = {
      email: $.email.value,
      password: $.password.value };

    Alloy.m.apiSend('signin', params, 'POST', function (res) {
      if (res.status) {
        var dm = require('/dbManager');
        dm.setModel('user', {
          user_id: res.data.id,
          email: res.data.email,
          mobile: res.data.mobile,
          firstname: res.data.firstname,
          lastname: res.data.lastname,
          gender: res.data.gender,
          address: res.data.address,
          city: res.data.city,
          zip: res.data.zip,
          birthday: res.data.birthday,
          apitoken: res.data.apitoken,
          card: res.data.card });


        var win = Alloy.createController('/dashboard').getView();
        win.open();

      } else {
        alert(res.message);
      }
    });
  }
  function openSignup()
  {
    var win = Alloy.createController('/signup').getView();
    $.index.openWindow(win);
  }

  $.index.open();

  // Generated code that must be executed after all UI and
  // controller code. One example deferred event handlers whose
  // functions are not defined until after the controller code
  // is executed.
  __defers['$.__views["__alloyId62"]!click!Signin'] && $.addListener($.__views["__alloyId62"], 'click', Signin);__defers['$.__views["__alloyId66"]!click!openSignup'] && $.addListener($.__views["__alloyId66"], 'click', openSignup);

  // Extend the $ instance with all functions and properties
  // defined on the exports object.
  _.extend($, exports);
}

module.exports = Controller;
//# sourceMappingURL=file:///Users/roberto/Documents/Appcelerator_Studio_Workspace/LoyaltyPlatform/build/map/Resources/android/alloy/controllers/signin.js.map
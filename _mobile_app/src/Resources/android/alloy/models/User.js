var Alloy = require('/alloy'),
_ = require("/alloy/underscore")._,
model,collection;

exports.definition = {
  config: {
    columns: {
      "user_id": "INTEGER PRIMARY KEY",
      "firstname": "TEXT",
      "lastname": "TEXT",
      "gender": "TEXT",
      "birthday": "TEXT",
      "mobile": "TEXT",
      "email": "TEXT",
      "balance": "INTEGER",
      "city": "TEXT",
      "address": "TEXT",
      "zip": "TEXT",
      "apitoken": "TEXT",
      "card": "TEXT" },

    adapter: {
      type: "sql",
      collection_name: "user",
      db_name: Alloy.m.mainDbName,
      idAttribute: "user_id",
      remoteBackup: false } },


  extendModel: function (Model) {
    _.extend(Model.prototype, {
      // extended functions and properties go here
    });

    return Model;
  },
  extendCollection: function (Collection) {
    _.extend(Collection.prototype, {
      transform: function ()
      {
        var t = this.toJSON();

        if (t.gender == 'f') {
          t.gender_ft = 'Donna';
        } else if (t.gender == 'm') {
          t.gender_ft = 'Uomo';
        } else {
          t.gender_ft = '-';
        }
        if (t.birthday.length > 0) {
          var y = t.birthday.substring(0, 4);
          var m = t.birthday.substring(5, 2);
          var d = t.birthday.substring(8, 2);
          t.birthday_ft = d + '-' + m + '-' + y;
        }

        return t;
      }
      // extended functions and properties go here

      // For Backbone v1.1.2, uncomment the following to override the
      // fetch method to account for a breaking change in Backbone.
      /*
      fetch: function(options) {
      	options = options ? _.clone(options) : {};
      	options.reset = true;
      	return Backbone.Collection.prototype.fetch.call(this, options);
      }
      */ });


    return Collection;
  } };



model = Alloy.M('User',
exports.definition,
[]);


collection = Alloy.C('User',
exports.definition,
model);


exports.Model = model;
exports.Collection = collection;